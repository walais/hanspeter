import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:muslim_quiz_v1/ahadith.dart';
import 'card.dart';
import 'preferences.dart';
import 'questions.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  ScanResult scanResult;
  List<String> _cardList;
  List<String> _itemMenu = ["Hilfe", "Fragen"];

  @override
  void initState() {
    _cardList = Preferences().data;
    super.initState();
  }

  void updateCardList() {
    if (!_cardList.contains(scanResult.rawContent)) {
      _cardList.add(scanResult.rawContent);
      Preferences().data = _cardList;
      setState(() {
        _cardList = Preferences().data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> hisnulMuslim = new Ahadith().getHadith();
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                if (_cardList.length >= 52) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return certificate();
                      });
                }
              },
              child: Center(
                  child: Text(
                _cardList.length.toString() + "/52",
                style: TextStyle(fontSize: 16),
              )),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16),
              child: IconButton(
                  icon: Icon(Icons.help),
                  onPressed: () {
                    showDialog(
                        context: context,
                        child: AlertDialog(
                            content: Text(
                                "Klicke unten auf das Logo um den Scanner zu aktivieren")));
                  }),
            )
          ],
        ),
        body: Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(32),
          child: Stack(fit: StackFit.expand, children: [
            Image.asset(
              "assets/image/bg.png",
              fit: BoxFit.cover,
            ),
            Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(hisnulMuslim[0],
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 170, 0),
                              fontSize: 24,
                              fontWeight: FontWeight.bold)),
                      Expanded(
                          child: SingleChildScrollView(
                              child: Text(hisnulMuslim[1],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                  )))),
                      GestureDetector(
                        child: Image.asset("assets/image/logo.png",
                            isAntiAlias: true, scale: 2),
                        onTap: () {
                          scan();
                        },
                      )
                    ]))
          ]),
        ),
        floatingActionButton: floatingActionButton());
  }

  Widget floatingActionButton() {
    if (_cardList.length > 0) {
      return FloatingActionButton(
        child: Icon(Icons.style),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Questions()));
        },
      );
    }
  }

  Widget certificate() {
      return Card(
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5,
          margin: EdgeInsets.all(32),
          child: Image.asset(
            "assets/image/certificate.png",
            fit: BoxFit.cover,
          ));

  }

  Widget dropDown() {
    return DropdownButton<String>(
        items: _itemMenu.map((String dropDownItem) {
          return DropdownMenuItem<String>(
            value: dropDownItem,
            child: Text(dropDownItem),
          );
        }).toList(),
        onChanged: (String newValueSelected) {
          setState(() {});
        },
        icon: Icon(Icons.help));
  }

  Future scan() async {
    try {
      var result = await BarcodeScanner.scan();
      setState(() => {
            scanResult = result,
            if (scanResult != null && scanResult.rawContent.contains("G01"))
              {
                updateCardList(),
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Cards(id: scanResult.rawContent)))
              }
          });
    } on PlatformException catch (e) {
      var result =
          ScanResult(type: ResultType.Error, format: BarcodeFormat.unknown);
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = "No camera permission!";
        });
      } else {
        result.rawContent = "Unknown error: $e";
      }
      setState(() {
        scanResult = result;
      });
    }
  }
}
