
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:muslim_quiz_v1/cardsList.dart';

class Cards extends StatefulWidget {
  final String id;

  Cards({Key key, this.id}) : super(key: key);

  @override
  _CardState createState() => _CardState();
}

class _CardState extends State<Cards> with WidgetsBindingObserver {
  AudioPlayer audioPlayer;
  AudioCache audioCache;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    print("init");
    initPlayer();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    audioPlayer.stop();
    audioCache.clearCache();
    print("dispose");
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.inactive ||
        state == AppLifecycleState.paused) {
      audioPlayer.pause();
    } else if (state == AppLifecycleState.resumed) {
      audioPlayer.resume();
    }
  }

  initPlayer() {
    audioPlayer = new AudioPlayer();
    audioCache = new AudioCache(fixedPlayer: audioPlayer);
  }

  ///***********************************************************

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    Map<String, String> card = getCardNumber();
    return Scaffold(
        appBar: AppBar(
          title: Align(alignment: Alignment.centerRight, child: Text("Lösung")),
        ),
        body: Card(
          color: Colors.orangeAccent[200],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 1,
          margin: EdgeInsets.fromLTRB(16, 32, 16, 32),
          child: Padding(
              padding: EdgeInsets.fromLTRB(16, 32, 16, 32),
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Text(card["surah"] + ":" + card["ayah"],
                      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  Text(card["arabic"],
                      textAlign: TextAlign.end, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  Text(card["german"], style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  Divider(color: Colors.black),
                  localAsset(widget.id)
                ],
              ))),
        ));
  }

  ///***********************************************************

  Map<String, String> getCardNumber() {
    String card = widget.id.substring(3);
    int no = int.parse(card) - 1;
    return CardsList().getCardfromList(no);
  }

  ///***********************************************************

  Widget localAsset(String audio) {
    String path = "audio/" + audio.toLowerCase() + ".mp3";
    return _tab([
      _btn("Play", () => {audioCache.play(path)}),
      _btn("Pause", () => {audioPlayer.pause()})
    ]);
  }

  Widget _tab(List<Widget> children) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: children
              .map((w) => Container(child: w, padding: EdgeInsets.all(6.0)))
              .toList(),
        ),
      ),
    );
  }

  Widget _btn(String txt, VoidCallback onPressed) {
    return ButtonTheme(
        minWidth: 48.0,
        child: RaisedButton(color: Colors.cyan[900],child: Text(txt),textColor: Colors.white, onPressed: onPressed));
  }
}
