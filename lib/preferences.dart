import 'package:shared_preferences/shared_preferences.dart';

class Preferences {

  static final Preferences _instance = Preferences._ctor();
  factory Preferences(){
    return _instance;
  }

  Preferences._ctor();

  SharedPreferences _prefs;

  init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  get data {
    return _prefs.getStringList("data") ?? <String>[];
  }

  set data(List<String> value){
    _prefs.setStringList("data", value);
  }

  get trophy {
    return _prefs.getBool("trophy") ?? false;
  }

  set trophy(bool value){
    _prefs.setBool("trophy", value);
  }

}