import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share/share.dart';

class Trophy extends StatefulWidget {
  final String question;

  Trophy({Key key, this.question}) : super(key: key);

  @override
  _TrophyState createState() => _TrophyState();
}

class _TrophyState extends State<Trophy> {

  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
                icon: Icon(Icons.share),
                onPressed: () {
                  screenshotController
                      .capture()
                      .then((File image) {
                    setState(() {
                      _imageFile = image;
                      Share.shareFiles([_imageFile.path]);
                      print(_imageFile.path);
                    });
                  }).catchError((onError) {
                    print(onError);
                  });
                })
          ],
        ),
        body: Screenshot(
            controller: screenshotController, child: Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 5,
            margin: EdgeInsets.all(8),
            child: Stack(fit: StackFit.expand, children: [
              Image.asset(
              "assets/image/trophy.png",
              fit: BoxFit.fitWidth,
            ),
              Center(child:Padding(
                padding: EdgeInsets.all(16),
                child: Text(widget.question,
                    style: TextStyle(
                        color: Color.fromARGB(255, 255, 170, 0),
                        fontSize: 24,
                        fontWeight: FontWeight.bold)),
              ))
            ]))));
  }
}
