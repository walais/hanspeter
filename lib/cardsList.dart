class CardsList {

  Map<String, String> getCardfromList(int i){
    return cards[i];
  }

  int getCardsSize(){
    return cards.length;
  }

  final List<Map<String, String>> cards = [
  {
  "arabic": "وَلَبِثُوا فِي كَهْفِهِمْ ثَلَاثَ مِائَةٍ سِنِينَ وَازْدَادُوا تِسْعًا",
  "german": "Und sie verweilten in ihrer Höhle dreihundert Jahre und noch neun dazu.",
  "id": "G0101",
  "surah": "18",
  "ayah": "25"
  },
  {
  "arabic": "وَاللَّـهُ خَلَقَ كُلَّ دَابَّةٍ مِّن مَّاءٍ ۖ فَمِنْهُم مَّن يَمْشِي عَلَىٰ بَطْنِهِ وَمِنْهُم مَّن يَمْشِي عَلَىٰ رِجْلَيْنِ وَمِنْهُم مَّن يَمْشِي عَلَىٰ أَرْبَعٍ ۚ يَخْلُقُ اللَّـهُ مَا يَشَاءُ ۚ إِنَّ اللَّـهَ عَلَىٰ كُلِّ شَيْءٍ قَدِيرٌ",
  "german": "Und Allah hat jedes Tier aus Wasser erschaffen. So gibt es unter ihnen solche, die auf ihrem Bauch kriechen, und solche, die auf zwei Beinen gehen, und (wieder) solche, die auf Vieren gehen. Allah erschafft, was Er will. Gewiß, Allah hat zu allem die Macht.",
  "id": "G0102",
  "surah": "24",
  "ayah": "45"
  },
  {
  "arabic": "وَاصْنَعِ الْفُلْكَ بِأَعْيُنِنَا وَوَحْيِنَا وَلَا تُخَاطِبْنِي فِي الَّذِينَ ظَلَمُوا ۚ إِنَّهُم مُّغْرَقُونَ ﴿٣٧﴾ وَيَصْنَعُ الْفُلْكَ وَكُلَّمَا مَرَّ عَلَيْهِ مَلَأٌ مِّن قَوْمِهِ سَخِرُوا مِنْهُ ۚ قَالَ إِن تَسْخَرُوا مِنَّا فَإِنَّا نَسْخَرُ مِنكُمْ كَمَا تَسْخَرُونَ",
  "german": "Und verfertige das Schiff vor Unseren Augen und nach Unserer Eingebung. Und sprich Mich nicht an zugunsten derer, die Unrecht getan haben; sie werden ertränkt werden. Er verfertigte das Schiff, wobei jedesmal, wenn eine führende Schar aus seinem Volk an ihm vorbeikam, sie über ihn spotteten. Er sagte: \"Wenn ihr über uns spottet, werden auch wir über euch spotten, so wie ihr (jetzt über uns) spottet.\"",
  "id": "G0103",
  "surah": "11",
  "ayah": "(37-38)"
  },
  {
  "arabic": "وَأُتْبِعُوا فِي هَـٰذِهِ الدُّنْيَا لَعْنَةً وَيَوْمَ الْقِيَامَةِ ۗ أَلَا إِنَّ عَادًا كَفَرُوا رَبَّهُمْ ۗ أَلَا بُعْدًا لِّعَادٍ قَوْمِ هُودٍ",
  "german": "Aber ein Fluch folgte ihnen im Diesseits nach und (wird ihnen) am Tag der Auferstehung (nachfolgen). Sicherlich, die 'Ad verleugneten ihren Herrn. Aber ja, weg mit 'Ad, dem Volk von Hud!",
  "id": "G0104",
  "surah": "11",
  "ayah": "60"
  },
  {
  "arabic": "فَلَمَّا ذَهَبُوا بِهِ وَأَجْمَعُوا أَن يَجْعَلُوهُ فِي غَيَابَتِ الْجُبِّ ۚ وَأَوْحَيْنَا إِلَيْهِ لَتُنَبِّئَنَّهُم بِأَمْرِهِمْ هَـٰذَا وَهُمْ لَا يَشْعُرُونَ",
  "german": "Als sie ihn mitnahmen und sich geeinigt hatten, ihn in die verborgene Tiefe des Brunnenlochs zu stecken, gaben Wir ihm ein: \"Du wirst ihnen ganz gewiß noch diese ihre Tat kundtun, ohne dass sie merken.\"",
  "id": "G0105",
  "surah": "12",
  "ayah": "15"
  },
  {
  "arabic": "إِذْ قَالَ يُوسُفُ لِأَبِيهِ يَا أَبَتِ إِنِّي رَأَيْتُ أَحَدَ عَشَرَ كَوْكَبًا وَالشَّمْسَ وَالْقَمَرَ رَأَيْتُهُمْ لِي سَاجِدِينَ",
  "german": " Als Yusuf zu seinem Vater sagte: \"O mein Vater, ich sah elf Sterne und die Sonne und den Mond, ich sah sie sich vor mir niederwerfen.\"",
  "id": "G0106",
  "surah": "12",
  "ayah": "4"
  },
  {
  "arabic": "وَقَالَ الْمَلِكُ إِنِّي أَرَىٰ سَبْعَ بَقَرَاتٍ سِمَانٍ يَأْكُلُهُنَّ سَبْعٌ عِجَافٌ وَسَبْعَ سُنبُلَاتٍ خُضْرٍ وَأُخَرَ يَابِسَاتٍ ۖ يَا أَيُّهَا الْمَلَأُ أَفْتُونِي فِي رُؤْيَايَ إِن كُنتُمْ لِلرُّؤْيَا تَعْبُرُونَ",
  "german": "Und der König sagte: \"Ich sah sieben fette Kühe, die von sieben mageren gefressen wurden, und sieben grüne Ähren und (sieben) andere dürre. O ihr führende Schar, gebt mir Auskunft über mein (Traum)gesicht, wenn ihr ein (Traum)gesicht auslegen könnt.\"",
  "id": "G0107",
  "surah": "12",
  "ayah": "43"
  },
  {
  "arabic": "الْحَمْدُ لِلَّـهِ الَّذِي وَهَبَ لِي عَلَى الْكِبَرِ إِسْمَاعِيلَ وَإِسْحَاقَ ۚ إِنَّ رَبِّي لَسَمِيعُ الدُّعَاءِ ",
  "german": "(Alles) Lob gehört Allah, Der mir trotz meines hohen Alters Ismail und Ishaq geschenkt hat! Gewiß, mein Herr ist wahrlich der Erhörer des Gebets.",
  "id": "G0108",
  "surah": "14",
  "ayah": "39"
  },
  {
  "arabic": "قُلْ نَزَّلَهُ رُوحُ الْقُدُسِ مِن رَّبِّكَ بِالْحَقِّ لِيُثَبِّتَ الَّذِينَ آمَنُوا وَهُدًى وَبُشْرَىٰ لِلْمُسْلِمِينَ",
  "german": "Sag: Offenbart hat ihn der Heilige Geist von deinem Herrn mit der Wahrheit, um diejenigen, die glauben, zu festigen, und als Rechtleitung und frohe Botschaft für die (Allah) Ergebenen.",
  "id": "G0109",
  "surah": "16",
  "ayah": "102"
  },
  {
  "arabic": "فَوَجَدَا عَبْدًا مِّنْ عِبَادِنَا آتَيْنَاهُ رَحْمَةً مِّنْ عِندِنَا وَعَلَّمْنَاهُ مِن لَّدُنَّا عِلْمًا",
  "german": "Sie trafen einen von Unseren Dienern, dem Wir Barmherzigkeit von Uns aus hatten zukommen lassen und den Wir Wissen von Uns her gelehrt hatten.",
  "id": "G0110",
  "surah": "18",
  "ayah": "65"
  },
  {
  "arabic": "حَتَّىٰ إِذَا بَلَغَ بَيْنَ السَّدَّيْنِ وَجَدَ مِن دُونِهِمَا قَوْمًا لَّا يَكَادُونَ يَفْقَهُونَ قَوْلًا ﴿٩٣﴾ قَالُوا يَا ذَا الْقَرْنَيْنِ إِنَّ يَأْجُوجَ وَمَأْجُوجَ مُفْسِدُونَ فِي الْأَرْضِ فَهَلْ نَجْعَلُ لَكَ خَرْجًا عَلَىٰ أَن تَجْعَلَ بَيْنَنَا وَبَيْنَهُمْ سَدًّا",
  "german": "bis, als er den Ort zwischen den beiden Bergen erreichte, er diesseits von ihnen ein Volk fand, das beinahe kein Wort verstand. Sie sagten: \"O Du Dhel-Qarnain, Ya'gug und Ma'gug stiften Unheil auf der Erde. Sollen wir dir eine Gebühr dafür aussetzen, dass du zwischen uns und ihnen eine Sperrmauer errichtest?\"",
  "id": "G0111",
  "surah": "18",
  "ayah": "(93-94)"
  },
  {
  "arabic": "قَالَ إِنِّي عَبْدُ اللَّـهِ آتَانِيَ الْكِتَابَ وَجَعَلَنِي نَبِيًّا",
  "german": "Er sagte: \"Ich bin wahrlich Allahs Diener; Er hat mir die Schrift gegeben und mich zu einem Propheten gemacht.\"",
  "id": "G0112",
  "surah": "19",
  "ayah": "30"
  },
  {
  "arabic": "يَا زَكَرِيَّا إِنَّا نُبَشِّرُكَ بِغُلَامٍ اسْمُهُ يَحْيَىٰ لَمْ نَجْعَل لَّهُ مِن قَبْلُ سَمِيًّا ",
  "german": "O Zakariyya, Wir verkünden dir einen Jungen, dessen Name Yahya ist, wie Wir zuvor noch niemandem Kennzeichen gleich den seinen gegeben haben.",
  "id": "G0113",
  "surah": "19",
  "ayah": "7"
  },
  {
  "arabic": "وَإِذْ يَرْفَعُ إِبْرَاهِيمُ الْقَوَاعِدَ مِنَ الْبَيْتِ وَإِسْمَاعِيلُ رَبَّنَا تَقَبَّلْ مِنَّا ۖ إِنَّكَ أَنتَ السَّمِيعُ الْعَلِيمُ",
  "german": "Und (gedenkt), als Ibrahim die Grundmauern des Hauses errichtete, zusammen mit Isma'il (da beteten sie): \"Unser Herr, nimm (es) von uns an. Du bist ja der Allhörende und Allwissende.\"",
  "id": "G0114",
  "surah": "2",
  "ayah": "127"
  },
  {
  "arabic": "فَهَزَمُوهُم بِإِذْنِ اللَّـهِ وَقَتَلَ دَاوُودُ جَالُوتَ وَآتَاهُ اللَّـهُ الْمُلْكَ وَالْحِكْمَةَ وَعَلَّمَهُ مِمَّا يَشَاءُ ۗ وَلَوْلَا دَفْعُ اللَّـهِ النَّاسَ بَعْضَهُم بِبَعْضٍ لَّفَسَدَتِ الْأَرْضُ وَلَـٰكِنَّ اللَّـهَ ذُو فَضْلٍ عَلَى الْعَالَمِينَ",
  "german": "Und so schlugen sie sie mit Allahs Erlaubnis, und Dawud tötete Galut. Und Allah gab ihm die Herrschaft und die Weisheit und lehrte ihn von dem, was Er wollte. Und wenn nicht Allah die einen Menschen durch die anderen zurückweisen würde, geriete die Erde wahrlich ins Verderben. Aber Allah ist voll Huld gegen die Weltenbewohner.",
  "id": "G0115",
  "surah": "2",
  "ayah": "251"
  },
  {
  "arabic": "وَإِذْ قَالَ إِبْرَاهِيمُ رَبِّ أَرِنِي كَيْفَ تُحْيِي الْمَوْتَىٰ ۖ قَالَ أَوَلَمْ تُؤْمِن ۖ قَالَ بَلَىٰ وَلَـٰكِن لِّيَطْمَئِنَّ قَلْبِي ۖ قَالَ فَخُذْ أَرْبَعَةً مِّنَ الطَّيْرِ فَصُرْهُنَّ إِلَيْكَ ثُمَّ اجْعَلْ عَلَىٰ كُلِّ جَبَلٍ مِّنْهُنَّ جُزْءًا ثُمَّ ادْعُهُنَّ يَأْتِينَكَ سَعْيًا ۚ وَاعْلَمْ أَنَّ اللَّـهَ عَزِيزٌ حَكِيمٌ",
  "german": "Und als Ibrahim sagte: \"Mein Herr, zeige mir, wie Du die Toten lebendig machst!\" Er sagte: \"Glaubst du immer noch nicht?\" Er sagte: \"Doch, aber (ich frage,) damit mein Herz Ruhe findet.\" Er (Allah) sagte: \"So nimm vier von den Vögeln und zieh sie dann her zu dir. Hierauf setze auf jeden Berg einen Teil von ihnen. Hierauf rufe sie, so werden sie zu dir herbeigeeilt kommen. Und wisse, daß Allah Allmächtig und Allweise ist.\"",
  "id": "G0116",
  "surah": "2",
  "ayah": "260"
  },
  {
  "arabic": "وَإِذْ قُلْنَا لِلْمَلَائِكَةِ اسْجُدُوا لِآدَمَ فَسَجَدُوا إِلَّا إِبْلِيسَ أَبَىٰ وَاسْتَكْبَرَ وَكَانَ مِنَ الْكَافِرِينَ",
  "german": " Und als Wir zu den Engeln sagten: \"Werft euch vor Adam nieder!\" Da warfen sie sich nieder, außer Iblis. Er weigerte sich und verhielt sich hochmütig und gehörte zu den Ungläubigen.",
  "id": "G0117",
  "surah": "2",
  "ayah": "34"
  },
  {
  "arabic": "وَقُلْنَا يَا آدَمُ اسْكُنْ أَنتَ وَزَوْجُكَ الْجَنَّةَ وَكُلَا مِنْهَا رَغَدًا حَيْثُ شِئْتُمَا وَلَا تَقْرَبَا هَـٰذِهِ الشَّجَرَةَ فَتَكُونَا مِنَ الظَّالِمِينَ ﴿٣٥﴾ فَأَزَلَّهُمَا الشَّيْطَانُ عَنْهَا فَأَخْرَجَهُمَا مِمَّا كَانَا فِيهِ ۖ وَقُلْنَا اهْبِطُوا بَعْضُكُمْ لِبَعْضٍ عَدُوٌّ ۖ وَلَكُمْ فِي الْأَرْضِ مُسْتَقَرٌّ وَمَتَاعٌ إِلَىٰ حِينٍ",
  "german": "Und Wir sagten: \"O Adam, bewohne du und deine Gattin den (Paradies)garten, und eßt von ihm reichlich, wo immer ihr wollt! Aber naht euch nicht diesem Baum, sonst gehört ihr zu den Ungerechten!\" Doch Satan entfernte sie davon, und da vertrieb er sie aus dem, worin sie (an Glückseligkeit) gewesen waren. Wir sagten: \"Geht fort! Einige von euch seien der anderen Feind. Und auf der Erde sollt ihr Aufenthalt und Nießbrauch auf Zeit haben.\"",
  "id": "G0118",
  "surah": "2",
  "ayah": "(35-36)"
  },
  {
  "arabic": "وَإِذْ وَاعَدْنَا مُوسَىٰ أَرْبَعِينَ لَيْلَةً ثُمَّ اتَّخَذْتُمُ الْعِجْلَ مِن بَعْدِهِ وَأَنتُمْ ظَالِمُونَ",
  "german": "Und als Wir Uns mit Musa auf vierzig Nächte verabredeten, da nahmt ihr dann nach ihm das Kalb an, womit ihr Unrecht tatet.",
  "id": "G0119",
  "surah": "2",
  "ayah": "51"
  },
  {
  "arabic": "وَلَقَدْ عَلِمْتُمُ الَّذِينَ اعْتَدَوْا مِنكُمْ فِي السَّبْتِ فَقُلْنَا لَهُمْ كُونُوا قِرَدَةً خَاسِئِينَ",
  "german": "Und ihr kennt doch diejenigen von euch, die den Sabbat übertraten. Da sagten Wir zu ihnen: \"Werdet verstoßene Affen!\"",
  "id": "G0120",
  "surah": "2",
  "ayah": "65"
  },
  {
  "arabic": "إِنِّي أَنَا رَبُّكَ فَاخْلَعْ نَعْلَيْكَ ۖ إِنَّكَ بِالْوَادِ الْمُقَدَّسِ طُوًى",
  "german": "Gewiß, Ich bin dein Herr, so ziehe deine Schuhe aus. Du befindest dich im geheiligten Tal Tuwa.",
  "id": "G0121",
  "surah": "20",
  "ayah": "12"
  },
  {
  "arabic": "إِذْ أَوْحَيْنَا إِلَىٰ أُمِّكَ مَا يُوحَىٰ ﴿٣٨﴾ أَنِ اقْذِفِيهِ فِي التَّابُوتِ فَاقْذِفِيهِ فِي الْيَمِّ فَلْيُلْقِهِ الْيَمُّ بِالسَّاحِلِ يَأْخُذْهُ عَدُوٌّ لِّي وَعَدُوٌّ لَّهُ ۚ وَأَلْقَيْتُ عَلَيْكَ مَحَبَّةً مِّنِّي وَلِتُصْنَعَ عَلَىٰ عَيْنِي",
  "german": "als Wir deiner Mutter eingaben, was (als Weisung) eingegeben werden sollte: 'Wirf ihn in den Kasten und wirf ihn ins Wasser', und das Wasser soll ihn ans Ufer setzen, so daß ihn ein Feind von Mir und ein Feind von ihm aufnimmt. Und Ich habe auf dich Liebe von Mir gelegt - und damit du vor Meinem Auge aufgezogen würdest.",
  "id": "G0122",
  "surah": "20",
  "ayah": "(38-39)"
  },
  {
  "arabic": "إِذْ تَمْشِي أُخْتُكَ فَتَقُولُ هَلْ أَدُلُّكُمْ عَلَىٰ مَن يَكْفُلُهُ ۖ فَرَجَعْنَاكَ إِلَىٰ أُمِّكَ كَيْ تَقَرَّ عَيْنُهَا وَلَا تَحْزَنَ ۚ وَقَتَلْتَ نَفْسًا فَنَجَّيْنَاكَ مِنَ الْغَمِّ وَفَتَنَّاكَ فُتُونًا ۚ فَلَبِثْتَ سِنِينَ فِي أَهْلِ مَدْيَنَ ثُمَّ جِئْتَ عَلَىٰ قَدَرٍ يَا مُوسَىٰ",
  "german": "Als deine Schwester hinging und sagte: \"Soll ich euch auf jemanden hinweisen, der ihn betreuen würde?\" So gaben Wir dich deiner Mutter wieder, damit sie frohen Mutes und nicht traurig sei. Und du tötetest eine Seele, und da erretteten Wir dich aus dem Kummer, und Wir unterzogen dich einer harten Prüfung. So verweiltest du jahrelang unter den Leuten von Madyan. Hierauf kamst du zu einer vorausbestimmten Zeit, o Musa.",
  "id": "G0123",
  "surah": "20",
  "ayah": "40"
  },
  {
  "arabic": "وَذَا النُّونِ إِذ ذَّهَبَ مُغَاضِبًا فَظَنَّ أَن لَّن نَّقْدِرَ عَلَيْهِ فَنَادَىٰ فِي الظُّلُمَاتِ أَن لَّا إِلَـٰهَ إِلَّا أَنتَ سُبْحَانَكَ إِنِّي كُنتُ مِنَ الظَّالِمِينَ ﴿٨٧﴾ فَاسْتَجَبْنَا لَهُ وَنَجَّيْنَاهُ مِنَ الْغَمِّ ۚ وَكَذَٰلِكَ نُنجِي الْمُؤْمِنِينَ",
  "german": "Und (auch) dem Mann mit dem Fisch, als er erzürnt wegging. Da meinte er, Wir würden ihm nicht (den Lebensunterhalt) bemessen. Dann rief er in den Finsternissen: \"Es gibt keinen Gott außer Dir! Preis sei Dir! Gewiß, ich gehöre zu den Ungerechten.\" Da erhörten Wir ihn und erretteten ihn aus dem Kummer. So retten Wir die Gläubigen.",
  "id": "G0124",
  "surah": "21",
  "ayah": "(87-88)"
  },
  {
  "arabic": "فَأَنجَيْنَاهُ وَمَن مَّعَهُ فِي الْفُلْكِ الْمَشْحُونِ ﴿١١٩﴾ ثُمَّ أَغْرَقْنَا بَعْدُ الْبَاقِينَ",
  "german": "Da retteten Wir ihn und wer mit ihm war im vollbeladenen Schiff. Hierauf ließen Wir alsdann die übrigen ertrinken. ",
  "id": "G0125",
  "surah": "26",
  "ayah": "(119-120)"
  },
  {
  "arabic": "قَالَ أَلَمْ نُرَبِّكَ فِينَا وَلِيدًا وَلَبِثْتَ فِينَا مِنْ عُمُرِكَ سِنِينَ",
  "german": "Er (Fir'aun) sagte: \"Haben wir dich nicht als kleines Kind unter uns aufgezogen, und hast du dich nicht (viele) Jahre deines Lebens unter uns aufgehalten?\"",
  "id": "G0126",
  "surah": "26",
  "ayah": "18"
  },
  {
  "arabic": "وَوَهَبْنَا لَهُ مِن رَّحْمَتِنَا أَخَاهُ هَارُونَ نَبِيًّا",
  "german": "Und Wir schenkten ihm aus Unserer Barmherzigkeit seinen Bruder Harun als Propheten.",
  "id": "G0127",
  "surah": "19",
  "ayah": "53"
  },
  {
  "arabic": "فَأَوْحَيْنَا إِلَىٰ مُوسَىٰ أَنِ اضْرِب بِّعَصَاكَ الْبَحْرَ ۖ فَانفَلَقَ فَكَانَ كُلُّ فِرْقٍ كَالطَّوْدِ الْعَظِيمِ",
  "german": "Da gaben Wir Musa ein: \"Schlag mit deinem Stock auf das Meer.\" So spaltete es sich, und jeder Teil war wie ein gewaltiger Berg.",
  "id": "G0128",
  "surah": "26",
  "ayah": "63"
  },
  {
  "arabic": "إِنَّهُ مِن سُلَيْمَانَ وَإِنَّهُ بِسْمِ اللَّـهِ الرَّحْمَـٰنِ الرَّحِيمِ",
  "german": "Gewiß, es ist von Sulaiman, und es lautet: 'Im Namen Allahs, des Allerbarmers, des Barmherzigen.'",
  "id": "G0129",
  "surah": "27",
  "ayah": "30"
  },
  {
  "arabic": "وَوَرِثَ سُلَيْمَانُ دَاوُودَ ۖ وَقَالَ يَا أَيُّهَا النَّاسُ عُلِّمْنَا مَنطِقَ الطَّيْرِ وَأُوتِينَا مِن كُلِّ شَيْءٍ ۖ إِنَّ هَـٰذَا لَهُوَ الْفَضْلُ الْمُبِينُ",
  "german": "Und Sulaiman beerbte Dawud und sagte: \"O ihr Menschen, uns ist die Sprache der Vögel gelehrt worden, und uns wurde von allem gegeben. Das ist wahrlich die deutliche Huld.\"",
  "id": "G0130",
  "surah": "27",
  "ayah": "16"
  },
  {
  "arabic": "وَوَرِثَ سُلَيْمَانُ دَاوُودَ ۖ وَقَالَ يَا أَيُّهَا النَّاسُ عُلِّمْنَا مَنطِقَ الطَّيْرِ وَأُوتِينَا مِن كُلِّ شَيْءٍ ۖ إِنَّ هَـٰذَا لَهُوَ الْفَضْلُ الْمُبِينُ",
  "german": "Und Sulaiman beerbte Dawud und sagte: \"O ihr Menschen, uns ist die Sprache der Vögel gelehrt worden, und uns wurde von allem gegeben. Das ist wahrlich die deutliche Huld.\"",
  "id": "G0131",
  "surah": "27",
  "ayah": "16"
  },
  {
  "arabic": "وَحُشِرَ لِسُلَيْمَانَ جُنُودُهُ مِنَ الْجِنِّ وَالْإِنسِ وَالطَّيْرِ فَهُمْ يُوزَعُونَ",
  "german": "Und versammelt wurden für Sulaiman seine Heerscharen - unter den Ginn, Menschen und Vögeln -, und so wurden sie in Reihen geordnet.",
  "id": "G0132",
  "surah": "27",
  "ayah": "17"
  },
  {
  "arabic": "وَتَفَقَّدَ الطَّيْرَ فَقَالَ مَا لِيَ لَا أَرَى الْهُدْهُدَ أَمْ كَانَ مِنَ الْغَائِبِينَ",
  "german": "Und er schaute bei den Vögeln nach. Da sagte er: \"Wie kommt es, dass ich den Wiedehopf nicht sehe? Befindet er sich etwa unter den Abwesenden?\"",
  "id": "G0133",
  "surah": "27",
  "ayah": "20"
  },
  {
  "arabic": "قَالَ إِنِّي أُرِيدُ أَنْ أُنكِحَكَ إِحْدَى ابْنَتَيَّ هَاتَيْنِ عَلَىٰ أَن تَأْجُرَنِي ثَمَانِيَ حِجَجٍ ۖ فَإِنْ أَتْمَمْتَ عَشْرًا فَمِنْ عِندِكَ ۖ وَمَا أُرِيدُ أَنْ أَشُقَّ عَلَيْكَ ۚ سَتَجِدُنِي إِن شَاءَ اللَّـهُ مِنَ الصَّالِحِينَ",
  "german": "Er sagte: \"Ich will dich mit einer dieser meiner beiden Töchter verheiraten unter der Bedingung, daß du acht Jahre in meinen Dienst trittst. Wenn du sie aber auf zehn vollmachst, so steht es bei dir. Ich will dir keine Härte auferlegen. Du wirst mich, wenn Allah will, als einen der Rechtschaffenen finden.\"",
  "id": "G0134",
  "surah": "28",
  "ayah": "27"
  },
  {
  "arabic": "وَلَقَدْ أَرْسَلْنَا نُوحًا إِلَىٰ قَوْمِهِ فَلَبِثَ فِيهِمْ أَلْفَ سَنَةٍ إِلَّا خَمْسِينَ عَامًا فَأَخَذَهُمُ الطُّوفَانُ وَهُمْ ظَالِمُونَ",
  "german": "Und Wir sandten bereits Nuh zu seinem Volk. Er verweilte unter ihnen tausend Jahre weniger fünfzig Jahre. Da ergriff sie die Überschwemmung, während sie Unrecht taten.",
  "id": "G0135",
  "surah": "29",
  "ayah": "14"
  },
  {
  "arabic": "إِذْ قَالَتِ امْرَأَتُ عِمْرَانَ رَبِّ إِنِّي نَذَرْتُ لَكَ مَا فِي بَطْنِي مُحَرَّرًا فَتَقَبَّلْ مِنِّي ۖ إِنَّكَ أَنتَ السَّمِيعُ الْعَلِيمُ ﴿٣٥﴾ فَلَمَّا وَضَعَتْهَا قَالَتْ رَبِّ إِنِّي وَضَعْتُهَا أُنثَىٰ وَاللَّـهُ أَعْلَمُ بِمَا وَضَعَتْ وَلَيْسَ الذَّكَرُ كَالْأُنثَىٰ ۖ وَإِنِّي سَمَّيْتُهَا مَرْيَمَ وَإِنِّي أُعِيذُهَا بِكَ وَذُرِّيَّتَهَا مِنَ الشَّيْطَانِ الرَّجِيمِ ",
  "german": "Als 'Imrans Frau sagte: \"Mein Herr, ich gelobe Dir, was in meinem Mutterleib ist, für Deinen Dienst freigestellt. So nimm (es) von mir an! Du bist ja der Allhörende und Allwissende.\" Als sie sie dann zur Welt gebracht hatte, sagte sie: \"Mein Herr, ich habe ein Mädchen zur Welt gebracht.\" Und Allah wußte sehr wohl, was sie zur Welt gebracht hatte, und der Knabe ist nicht wie das Mädchen. \"Ich habe sie Maryam genannt, und ich stelle sie und ihre Nachkommenschaft unter Deinen Schutz vor dem gesteinigten Satan.\"",
  "id": "G0136",
  "surah": "3",
  "ayah": "(35-36)"
  },
  {
  "arabic": "وَرَسُولًا إِلَىٰ بَنِي إِسْرَائِيلَ أَنِّي قَدْ جِئْتُكُم بِآيَةٍ مِّن رَّبِّكُمْ ۖ أَنِّي أَخْلُقُ لَكُم مِّنَ الطِّينِ كَهَيْئَةِ الطَّيْرِ فَأَنفُخُ فِيهِ فَيَكُونُ طَيْرًا بِإِذْنِ اللَّـهِ ۖ وَأُبْرِئُ الْأَكْمَهَ وَالْأَبْرَصَ وَأُحْيِي الْمَوْتَىٰ بِإِذْنِ اللَّـهِ ۖ وَأُنَبِّئُكُم بِمَا تَأْكُلُونَ وَمَا تَدَّخِرُونَ فِي بُيُوتِكُمْ ۚ إِنَّ فِي ذَٰلِكَ لَآيَةً لَّكُمْ إِن كُنتُم مُّؤْمِنِينَ",
  "german": "Und (Er wird ihn schicken) als einen Gesandten zu den Kindern Isra'ils ( zu denen er sagen wird): \"Gewiß, ich bin ja mit einem Zeichen von eurem Herrn zu euch gekommen: dass ich euch aus Lehm (etwas) schaffe, (was so aussieht) wie die Gestalt eines Vogels, und dann werde ich ihm einhauchen, und da wird es ein (wirklicher) Vogel sein. Und ich werde mit Allahs Erlaubnis den Blindgeborenen und den Weißgefleckten heilen und werde Tote mit Allahs Erlaubnis wieder lebendig machen. Und ich werde euch kundtun, was ihr eßt und was ihr in euren Häusern aufspeichert. Darin ist wahrlich ein Zeichen für euch, wenn ihr gläubig seid.\"",
  "id": "G0137",
  "surah": "3",
  "ayah": "49"
  },
  {
  "arabic": "وَإِذْ قَالَت طَّائِفَةٌ مِّنْهُمْ يَا أَهْلَ يَثْرِبَ لَا مُقَامَ لَكُمْ فَارْجِعُوا ۚ وَيَسْتَأْذِنُ فَرِيقٌ مِّنْهُمُ النَّبِيَّ يَقُولُونَ إِنَّ بُيُوتَنَا عَوْرَةٌ وَمَا هِيَ بِعَوْرَةٍ ۖ إِن يُرِيدُونَ إِلَّا فِرَارًا",
  "german": "Und als eine Gruppe von ihnen sagte: \"O ihr Leute von Yatrib, ihr könnt euch (hier) nicht aufhalten. Kehrt zurück.\" Und ein Teil von ihnen bat den Propheten um Erlaubnis, (heimzukehren) indem sie sagten: \"Unsere Häuser sind ohne Schutz.\" Dabei waren sie nicht ohne Schutz, sie wollten nur fliehen.",
  "id": "G0138",
  "surah": "33",
  "ayah": "13"
  },
  {
  "arabic": "رَبِّ هَبْ لِي مِنَ الصَّالِحِينَ ﴿١٠٠﴾ فَبَشَّرْنَاهُ بِغُلَامٍ حَلِيمٍ ﴿١٠١﴾ فَلَمَّا بَلَغَ مَعَهُ السَّعْيَ قَالَ يَا بُنَيَّ إِنِّي أَرَىٰ فِي الْمَنَامِ أَنِّي أَذْبَحُكَ فَانظُرْ مَاذَا تَرَىٰ ۚ قَالَ يَا أَبَتِ افْعَلْ مَا تُؤْمَرُ ۖ سَتَجِدُنِي إِن شَاءَ اللَّـهُ مِنَ الصَّابِرِينَ",
  "german": "\"Mein Herr, schenke mir einen von den Rechtschaffenen\". Da verkündeten Wir ihm einen nachsichtigen Jungen. Als dieser das Alter erreichte, dass er mit ihm laufen konnte, sagte er: \"O mein lieber Sohn, ich sehe im Schlaf, daß ich dich schlachte. Schau jetzt, was du (dazu) meinst.\" Er sagte: \"O mein lieber Vater, tu, was dir befohlen wird. Du wirst mich, wenn Allah will, als einen der Standhaften finden.\"",
  "id": "G0139",
  "surah": "37",
  "ayah": "(100-102)"
  },
  {
  "arabic": "فَالْتَقَمَهُ الْحُوتُ وَهُوَ مُلِيمٌ",
  "german": "Da verschlang ihn der (große) Fisch, während er sich Tadel zugezogen hatte.",
  "id": "G0140",
  "surah": "37",
  "ayah": "142"
  },
  {
  "arabic": "وَمَنْ أَحْسَنُ دِينًا مِّمَّنْ أَسْلَمَ وَجْهَهُ لِلَّـهِ وَهُوَ مُحْسِنٌ وَاتَّبَعَ مِلَّةَ إِبْرَاهِيمَ حَنِيفًا ۗ وَاتَّخَذَ اللَّـهُ إِبْرَاهِيمَ خَلِيلً",
  "german": "Wer hätte eine bessere Religion, als wer sein Gesicht Allah hingibt und dabei Gutes tut und dem Glaubensbekenntnis Ibrahims folgt, (als) Anhänger des rechten Glaubens? Und Allah nahm sich Ibrahim zum Freund.",
  "id": "G0141",
  "surah": "4",
  "ayah": "125"
  },
  {
  "arabic": "وَضَرَبَ اللَّـهُ مَثَلًا لِّلَّذِينَ آمَنُوا امْرَأَتَ فِرْعَوْنَ إِذْ قَالَتْ رَبِّ ابْنِ لِي عِندَكَ بَيْتًا فِي الْجَنَّةِ وَنَجِّنِي مِن فِرْعَوْنَ وَعَمَلِهِ وَنَجِّنِي مِنَ الْقَوْمِ الظَّالِمِينَ",
  "german": "Und Allah hat als Gleichnis für diejenigen, die glauben, dasjenige von Fir'auns Frau geprägt. Als sie sagte: \"Mein Herr, baue mir bei Dir ein Haus im (Paradies)garten, und errette mich von Fir'aun und seinem Werk, und errette mich von dem Volk der Ungerechten.\"",
  "id": "G0142",
  "surah": "66",
  "ayah": "11"
  },
  {
  "arabic": "وَأَمَّا عَادٌ فَأُهْلِكُوا بِرِيحٍ صَرْصَرٍ عَاتِيَةٍ",
  "german": "Was aber die 'Ad angeht, so wurden sie durch einen heftig wehenden eiskalten Wind vernichtet",
  "id": "G0143",
  "surah": "69",
  "ayah": "6"
  },
  {
  "arabic": "فَأَلْقَىٰ عَصَاهُ فَإِذَا هِيَ ثُعْبَانٌ مُّبِينٌ",
  "german": "Er warf seinen Stock hin, und sogleich war er eine deutliche Schlange.",
  "id": "G0144",
  "surah": "7",
  "ayah": "107"
  },
  {
  "arabic": "وَإِلَىٰ ثَمُودَ أَخَاهُمْ صَالِحًا ۗ قَالَ يَا قَوْمِ اعْبُدُوا اللَّـهَ مَا لَكُم مِّنْ إِلَـٰهٍ غَيْرُهُ ۖ قَدْ جَاءَتْكُم بَيِّنَةٌ مِّن رَّبِّكُمْ ۖ هَـٰذِهِ نَاقَةُ اللَّـهِ لَكُمْ آيَةً ۖ فَذَرُوهَا تَأْكُلْ فِي أَرْضِ اللَّـهِ ۖ وَلَا تَمَسُّوهَا بِسُوءٍ فَيَأْخُذَكُمْ عَذَابٌ أَلِيمٌ",
  "german": "Und (Wir sandten) zu Tamud ihren Bruder Salih. Er sagte: \"O mein Volk, dient Allah! Keinen Gott habt ihr außer Ihm. Nun ist ein klarer Beweis von eurem Herrn zu euch gekommen: Dies ist die Kamelstute Allahs, euch zum Zeichen. So laßt sie auf Allahs Erde fressen und fügt ihr nichts Böses zu, sonst überkommt euch schmerzhafte Strafe.\"",
  "id": "G0145",
  "surah": "7",
  "ayah": "73"
  },
  {
  "arabic": "وَإِلَىٰ ثَمُودَ أَخَاهُمْ صَالِحًا ۗ قَالَ يَا قَوْمِ اعْبُدُوا اللَّـهَ مَا لَكُم مِّنْ إِلَـٰهٍ غَيْرُهُ ۖ قَدْ جَاءَتْكُم بَيِّنَةٌ مِّن رَّبِّكُمْ ۖ هَـٰذِهِ نَاقَةُ اللَّـهِ لَكُمْ آيَةً ۖ فَذَرُوهَا تَأْكُلْ فِي أَرْضِ اللَّـهِ ۖ وَلَا تَمَسُّوهَا بِسُوءٍ فَيَأْخُذَكُمْ عَذَابٌ أَلِيمٌ",
  "german": "Und (Wir sandten) zu Tamud ihren Bruder Salih. Er sagte: \"O mein Volk, dient Allah! Keinen Gott habt ihr außer Ihm. Nun ist ein klarer Beweis von eurem Herrn zu euch gekommen: Dies ist die Kamelstute Allahs, euch zum Zeichen. So laßt sie auf Allahs Erde fressen und fügt ihr nichts Böses zu, sonst überkommt euch schmerzhafte Strafe.\"",
  "id": "G0146",
  "surah": "7",
  "ayah": "73"
  },
  {
  "arabic": "إِلَّا تَنصُرُوهُ فَقَدْ نَصَرَهُ اللَّـهُ إِذْ أَخْرَجَهُ الَّذِينَ كَفَرُوا ثَانِيَ اثْنَيْنِ إِذْ هُمَا فِي الْغَارِ إِذْ يَقُولُ لِصَاحِبِهِ لَا تَحْزَنْ إِنَّ اللَّـهَ مَعَنَا ۖ فَأَنزَلَ اللَّـهُ سَكِينَتَهُ عَلَيْهِ وَأَيَّدَهُ بِجُنُودٍ لَّمْ تَرَوْهَا وَجَعَلَ كَلِمَةَ الَّذِينَ كَفَرُوا السُّفْلَىٰ ۗ وَكَلِمَةُ اللَّـهِ هِيَ الْعُلْيَا ۗ وَاللَّـهُ عَزِيزٌ حَكِيمٌ",
  "german": "Wenn ihr ihm nicht helft, so hat Allah ihm (schon damals) geholfen, als diejenigen, die ungläubig waren, ihn als einen von Zweien vertrieben; als sie beide in der Höhle waren und als er zu seinem Gefährten sagte: \"Sei nicht traurig! Gewiß, Allah ist mit uns!\" Da sandte Allah Seine innere Ruhe auf ihn herab und stärkte ihn mit Heerscharen, die ihr nicht saht, und erniedrigte das Wort derjenigen, die ungläubig waren, während Allahs Wort (doch) das hohe ist. Allah ist Allmächtig und Allweise.",
  "id": "G0147",
  "surah": "9",
  "ayah": "40"
  },
  {
  "arabic": "أَلَمْ تَرَ كَيْفَ فَعَلَ رَبُّكَ بِأَصْحَابِ الْفِيلِ ﴿١﴾ أَلَمْ يَجْعَلْ كَيْدَهُمْ فِي تَضْلِيلٍ ﴿٢﴾ وَأَرْسَلَ عَلَيْهِمْ طَيْرًا أَبَابِيلَ ﴿٣﴾ تَرْمِيهِم بِحِجَارَةٍ مِّن سِجِّيلٍ ﴿٤﴾ فَجَعَلَهُمْ كَعَصْفٍ مَّأْكُولٍ",
  "german": "Siehst du nicht, wie dein Herr mit den Leuten des Elefanten verfuhr? Ließ Er nicht ihre List verlorengehen und sandte gegen sie Vögel in aufeinanderfolgenden Schwärmen, die sie mit Steinen aus gebranntem Lehm bewarfen, und sie so wie abgefressene Halme machte?",
  "id": "G0148",
  "surah": "105",
  "ayah": "(1-5)"
  },
  {
  "arabic": "قُلْنَا يَا نَارُ كُونِي بَرْدًا وَسَلَامًا عَلَىٰ إِبْرَاهِيمَ",
  "german": "Wir sagten: \"O Feuer, sei Kühlung und Unversehrtheit für Ibrahim.\"",
  "id": "G0149",
  "surah": "21",
  "ayah": "69"
  },
  {
  "arabic": "وَإِذِ اسْتَسْقَىٰ مُوسَىٰ لِقَوْمِهِ فَقُلْنَا اضْرِب بِّعَصَاكَ الْحَجَرَ ۖ فَانفَجَرَتْ مِنْهُ اثْنَتَا عَشْرَةَ عَيْنًا ۖ قَدْ عَلِمَ كُلُّ أُنَاسٍ مَّشْرَبَهُمْ ۖ كُلُوا وَاشْرَبُوا مِن رِّزْقِ اللَّـهِ وَلَا تَعْثَوْا فِي الْأَرْضِ مُفْسِدِينَ",
  "german": "Und als Musa für sein Volk um Wasser bat, da sagten Wir: \"Schlag mit deinem Stock auf den Felsen!\" Da entsprangen ihm zwölf Quellen. Nun wußte jedermann, wo sein Platz zum Trinken war: \"Eßt und trinkt von Allahs versorgung und richtet auf der Erde nicht unheilstiftend Verderben an!\"",
  "id": "G0150",
  "surah": "2",
  "ayah": "60"
  },
  {
  "arabic": "وَكَذَٰلِكَ نُرِي إِبْرَاهِيمَ مَلَكُوتَ السَّمَاوَاتِ وَالْأَرْضِ وَلِيَكُونَ مِنَ الْمُوقِنِينَ ﴿٧٥﴾ فَلَمَّا جَنَّ عَلَيْهِ اللَّيْلُ رَأَىٰ كَوْكَبًا ۖ قَالَ هَـٰذَا رَبِّي ۖ فَلَمَّا أَفَلَ قَالَ لَا أُحِبُّ الْآفِلِينَ ﴿٧٦﴾ فَلَمَّا رَأَى الْقَمَرَ بَازِغًا قَالَ هَـٰذَا رَبِّي ۖ فَلَمَّا أَفَلَ قَالَ لَئِن لَّمْ يَهْدِنِي رَبِّي لَأَكُونَنَّ مِنَ الْقَوْمِ الضَّالِّينَ ﴿٧٧﴾ فَلَمَّا رَأَى الشَّمْسَ بَازِغَةً قَالَ هَـٰذَا رَبِّي هَـٰذَا أَكْبَرُ ۖ فَلَمَّا أَفَلَتْ قَالَ يَا قَوْمِ إِنِّي بَرِيءٌ مِّمَّا تُشْرِكُونَ ﴿٧٨﴾ إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِي فَطَرَ السَّمَاوَاتِ وَالْأَرْضَ حَنِيفًا ۖ وَمَا أَنَا مِنَ الْمُشْرِكِينَ",
  "german": "Und so zeigten Wir Ibrahim das Reich der Himmel und der Erde, - und damit er zu den Überzeugten gehöre. Als die Nacht über ihn hereinbrach, sah er einen Himmelskörper. Er sagte: \"Das ist mein Herr.\" Als er aber unterging, sagte er: \"Ich liebe nicht diejenigen, die untergehen.\" Als er dann den Mond aufgehen sah, sagte er: \"Das ist mein Herr.\" Als er aber unterging, sagte er: \"Wenn mein Herr mich nicht rechtleitet, werde ich ganz gewiß zum irregehenden Volk gehören.\" Als er dann die Sonne aufgehen sah, sagte er: \"Das ist mein Herr. Das ist größer.\" Als sie aber unterging, sagte er: \"O mein Volk, ich sage mich ja von dem los, was ihr (Ihm) beigesellt. Ich wende mein Gesicht Dem zu, Der die Himmel und die Erde erschaffen hat, als Anhänger des rechten Glaubens, und ich gehöre nicht zu den Götzendienern.\"",
  "id": "G0151",
  "surah": "6",
  "ayah": "(75-79)"
  },
  {
  "arabic": "تَبَّتْ يَدَا أَبِي لَهَبٍ وَتَبَّ ﴿١﴾ مَا أَغْنَىٰ عَنْهُ مَالُهُ وَمَا كَسَبَ ﴿٢﴾ سَيَصْلَىٰ نَارًا ذَاتَ لَهَبٍ",
  "german": "Zugrunde gehen sollen die Hände Abu Lahabs, und zugrunde gehen soll er (selbst)! Was nützt ihm sein Besitz und das, was er erworben hat? Er wird einem Feuer voller Flammen ausgesetzt sein.",
  "id": "G0152",
  "surah": "111",
  "ayah": "(1-3)"
  }
  ];
}