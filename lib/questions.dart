import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim_quiz_v1/preferences.dart';
import 'package:muslim_quiz_v1/trophy.dart';

class Questions extends StatefulWidget {
  Questions({Key key}) : super(key: key);

  @override
  _QuestionState createState() => _QuestionState();
}

class _QuestionState extends State<Questions> {
  List<String> _cardList;

  @override
  void initState() {
    _cardList = Preferences().data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text("Geschichte")),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help),
            onPressed: () {
              showDialog(
                  context: context,
                  child: AlertDialog(
                      content: Text(
                          "Wähle eine Frage aus und teile sie mit deinen Freunden!")));
            },
          )
        ],
      ),
      body: ListView.builder(
          itemCount: _cardList.length,
          itemBuilder: (context, index) {
            print(index);
            return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
                child: Card(
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.orangeAccent[200],
                  child: ListTile(
                      onTap: () {

                              Navigator.push(
                              context, MaterialPageRoute(builder: (context) => Trophy(question: questions[getNumber(index)])));
                              },
                      title: Text(
                        questions[getNumber(index)],
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      )),
                ));
          }),
    );
  }

  int getNumber(int index) {
    String cardName = _cardList[index];
    String cardNumber = cardName.substring(cardName.length - 2);
    return int.parse(cardNumber) - 1;
  }

  List<String> questions = [
    "Wie lange hat Allah eine Gruppe von Menschen in der Höhle schlafen lassen?",
    "Aus was wurden alle Tiere erschaffen?",
    "Welcher Prophet hat von Allah die Aufgabe bekommen, ein Schiff zu bauen?",
    "Welcher Prophet wurde zum Volk 'Ad gesandt?",
    "Welcher Prophet wurde in einen Brunnen geworfen?",
    "Welcher Prophet sagte folgende Worte zu seinem Vater: 'Oh mein Vater ich sah 11 Sterne und die Sonne und den Mond, ich sah sie sich vor mir niederwerfen.'?",
    "Wer träumte von 'Sieben fette Kühe, die von sieben mageren gefressen wurden, und sieben grüne Ähren und (sieben) andere dürre. '?",
    "Wie heißen die 2 Söhne von Ibrahim aleyhi selam?",
    "Von welchem Engel empfängt Muhammed ﷺ die Botschaft Allah's?",
    "Im Quran wird ein besonderer Diener Allah's erwähnt, dem Allah besonderes Wissen gegeben hat. Welcher Gesandter Allah's hat von ihm gelernt?",
    "Welchem Volk begegnete Dhul-Qarnain und errichtete eine Mauer, die uns Menschen bis zu einem bestimmten Tag vor diesem Volk schützen soll?",
    "Was waren die ersten Worte, die Isa aleyhi selam als Baby gesprochen hat?",
    "Wie heißt der Sohn von Zakariyya aleyhi selam?",
    "Wer erbaute die Kaaba in Mekka?",
    "Welchen großen und kräftigen Gegner besiegte Dawud aleyhi selam?",
    "Welcher Prophet hat das Wunder der 4 leblosen Vögel erlebt, welche zum Leben erweckt und zu ihm herbeigeeilt kamen?",
    "Welchen Befehl von Allah widersetzte sich Iblis?",
    "Warum wurden Adam aleyhi selam und seine Gattin aus dem Paradies fortgeschickt?",
    "Mit welchem Gesandten verabredete Sich Allah für 40 Nächte auf dem Berg Sinai?",
    "In was hat Allah die Menschen verwandelt, die das Sabbat-Gebot brachen?",
    "Was ist im heiligen Tal Tuwa geschehen?",
    "Welcher Gesandte wurde von seiner Mutter in einen Kasten gelegt und ins Wasser geworfen?",
    "Welcher Gesandte flüchtete nach Madyan?",
    "Welcher Prophet sagte folgende Worte: 'Es gibt keinen, der das Recht hat angebetet zu werden außer Dir! Preis sei Dir! Gewiß, ich gehöre zu den Ungerechten.'",
    "Das Volk von welchem Propheten wurde durch die Flut zerstört?",
    "Bei wem ist Musa aleyhi selam aufgewachsen?",
    "Wer war der Bruder von Musa aleyhi selam?",
    "Welcher Prophet teilte das Meer mit der Erlaubnis von Allah?",
    "Womit begann der Brief, den Sulayman aleyhi selam über den Wiedehopf an die Königin sandte?",
    "Welcher Prophet konnte mit Tieren reden?",
    "Wer war der Vater von Suleyman aleyhi selam?",
    "Welchem Propheten hat Allah die Macht über die Jinn gegeben?",
    "Wer oder was wurde Hud-hud genannt?",
    "Welche Bedingung hatte der Schwiegervater von Musa aleyhi selam für die Heirat seiner Tochter?",
    "Welcher Prophet verweilte 950 Jahre unter seinem Volk?",
    "Wie hieß der Vater von Meryem aleyhe selam?",
    "Welcher Prophet konnte mit der Zustimmung Allah's die Toten wieder zum Leben erwecken?",
    "Welchen anderen Namen hat die Stadt Medina gehabt, die im Quran erwähnt wird?",
    "Welcher Prophet bekam den Befehl seinen Sohn zu opfern?",
    "Welcher Prophet wurde vom Schiff geworfen und von einem großen Fisch verschlungen?",
    "Welchen Propheten hat Sich Allah zum Freund genommen?",
    "Welche Frau sprach folgendes Bittgebet: 'Mein Herr baue mir bei Dir ein Haus im Paradies, und errette mich von Fir'aun und seinem Werk, und errette mich von dem Volk der Ungerechten.'",
    "Mit welcher Strafe hat Allah das Volk 'Ad vernichtet?",
    "Bei welchem Propheten verwandelte sich der Stock in eine echte Schlange?",
    "Welcher Prophet wurde zum Volk Thamud gesandt?",
    "Welcher Prophet wird mit dem Kamel in Verbindung gebracht?",
    "RasulAllah ﷺ flüchtete vor den Leuten aus Mekka und er versteckte sich mit Abu Bakr in der Höhle. Was sagte Muhammed ﷺ zu Abu Bakr, als er sah, dass sich sein Gefährte Sorgen machte?",
    "In welcher Sura geht es um eine Riesenarmee, die sich auf den Weg macht die Kaba zu zerstören?",
    "Für welchen Propheten befahl Allah dem Feuer Kühlung und Unversehrheit zu sein?",
    "Bei welchem Propheten entsprangen 12 Quellen, indem er mit dem Stock auf einen Felsen schlug?",
    "Welcher Prophet hat sich in Aufrichtigkeit zu Allah gewandt, nachdem er den Stern, den Mond und die Sonne beobachtete?",
    "Nachdem eine Person den Gesandten Allah's ﷺ beleidigte, wurde eine Sura offenbart, dass diese Person mit seiner Frau im Gahannam sein wird. Wie hieß diese Person?"
  ];
}
