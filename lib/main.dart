import 'package:flutter/material.dart';
import 'package:muslim_quiz_v1/preferences.dart';
import 'package:muslim_quiz_v1/scan.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preferences().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Muslim Quiz',
      theme: ThemeData(
        primaryColor: Colors.cyan[900],
        accentColor: Colors.orangeAccent[400],
        fontFamily: "papyrus",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Muslim Quiz'),
    );
  }
}
